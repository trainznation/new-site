<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Input extends Component
{
    public $name;
    public $type;
    public $icon;
    public $label;
    /**
     * @var string
     */
    public $value;
    /**
     * @var bool
     */
    public $required;
    /**
     * @var bool
     */
    public $autofocus;
    /**
     * @var bool
     */
    public $horizontal;
    /**
     * @var bool
     */
    public $labeled;

    /**
     * Create a new component instance.
     *
     * @param $name
     * @param $type
     * @param $label
     * @param string $icon
     * @param string $value
     * @param bool $required
     * @param bool $autofocus
     * @param bool $horizontal
     * @param bool $labeled
     */
    public function __construct($name, $type, $label, $icon = '', $value = '', $required = false, $autofocus = false, $horizontal = false, $labeled = true)
    {
        $this->name = $name;
        $this->type = $type;
        $this->icon = $icon;
        $this->label = $label;
        $this->value = $value;
        $this->required = $required;
        $this->autofocus = $autofocus;
        $this->horizontal = $horizontal;
        $this->labeled = $labeled;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.input');
    }
}
