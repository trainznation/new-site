<?php

namespace App\Jobs\Auth;

use App\Events\Auth\RegisteredWelcome;
use App\Mail\Account\Registered;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class RegisteredUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    /**
     * Create a new job instance.
     *
     * @param $user
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Mail::to($this->user)->send(new Registered($this->user));
            event(new RegisteredWelcome($this->user));
        } catch (\Exception $exception) {
            Log::warning("Email de bienvenue non envoyer", ["error" => $exception]);
        }
    }
}
