<?php
if (!function_exists('currentRouteFront')) {
    /**
     * @param mixed ...$routes
     * @return string
     */
    function currentRouteFront(...$routes)
    {
        foreach ($routes as $route){
            if(\Illuminate\Support\Facades\Route::currentRouteName() === $route){
                return 'c-active';
            }
        }
    }
}
