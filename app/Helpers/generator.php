<?php
if (!function_exists('loginBackground')) {
    function loginBackground()
    {
        if(now() > "06:00" && now() < "18:00") {
            return '/assets/img/back_login.jpg';
        } else {
            return '/assets/img/back_login_night.jpg';
        }
    }
}

if (!function_exists('loginText')) {
    function loginText()
    {
        if(now() > "06:00" && now() < "19:00") {
            return "Bonjour !";
        } else {
            return "Bonsoir !";
        }
    }
}
