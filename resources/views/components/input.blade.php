<div class="form-group {{ $errors->has($name) ? 'has-error' : '' }}{{ $icon ? 'has-feedback' : '' }}">
    @if($labeled === true)
    <label for="{{ $name }}" class="control-label {{ $horizontal ? 'col-md-4' : '' }}">{{ $label }}</label>
    @endif
    @if($horizontal)
        <div class="col-md-6">
            <input
                type="{{$type}}"
                id="{{ $name }}"
                name="{{ $name }}"
                value="{{ old($name, $value !== '' ? $value : '') }}"
                class="form-control c-square c-theme {{ $icon ? 'input-lg' : '' }}"
                {{ $required ? 'required' : '' }}
                {{ $autofocus ? 'autofocus' : '' }}
                placeholder="{{ $label }}">
            @if($icon)
                <span class="{{ $icon }} form-control-feedback c-font-grey"></span>
            @endif
            <span class="c-font-red">{{ $errors->has($name) ? $errors->first($name) : '' }}</span>
        </div>
    @else
        <input
            type="{{$type}}"
            id="{{ $name }}"
            name="{{ $name }}"
            value="{{ old($name, $value !== '' ? $value : '') }}"
            class="form-control c-square c-theme {{ $icon ? 'input-lg' : '' }}"
            {{ $required ? 'required' : '' }}
            {{ $autofocus ? 'autofocus' : '' }}
            placeholder="{{ $label }}">
        @if($icon)
            <span class="{{ $icon }} form-control-feedback c-font-grey"></span>
        @endif
        <span class="c-font-red">{{ $errors->has($name) ? $errors->first($name) : '' }}</span>
    @endif
</div>
