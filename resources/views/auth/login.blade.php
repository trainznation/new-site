@extends('front.layout.app')

@section('content')
    <div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
        <div class="container">
            <div class="c-page-title c-pull-left">
                <h3 class="c-font-uppercase c-font-sbold">Connexion à votre compte</h3>
            </div>
            <ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
                <li><a href="{{ route('login') }}">Mon Compte</a></li>
                <li>/</li>
                <li class="c-state_active">Connexion</li>

            </ul>
        </div>
    </div>

    <div class="c-content-box c-size-md c-bgimage c-bg-img-center" style="background-image: url('{{ loginBackground() }}')">
        <div class="container c-margin-b-30 c-margin-t-30">
            <h1 class="c-font-bold text-center c-margin-b-60">{{ loginText() }}</h1>
            @if($errors->any())
                <div class="alert alert-danger" role="alert">
                    <div class="alert-text">
                        <h4 class="alert-heading">Erreur !</h4>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
            <div class="row c-padding-20">
                <div class="col-md-5" style="background: rgba(124,124,124,0.85); border: 1px solid #0d0e0f; border-radius: 5px; padding-top: 10px; padding-bottom: 10px;">
                    <h3 class="c-font-white">Connexion</h3>
                    <form id="formLogin" action="{{ route('login') }}" method="post">
                        @csrf
                        <div class="form-group has-feedback @error('email') has-error @enderror">
                            <input type="email" class="form-control c-square c-theme input-lg" placeholder="Email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            <span class="fa fa-envelope form-control-feedback c-font-grey"></span>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                            @enderror
                        </div>
                        <div class="form-group has-feedback @error('password') has-error @enderror">
                            <input type="password" class="form-control c-square c-theme input-lg" placeholder="Mot de Passe" name="password" required autocomplete="password" autofocus>
                            <span class="fa fa-key form-control-feedback c-font-grey"></span>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                            @enderror
                        </div>
                        <div class="form-group form-c-checkboxes">
                            <div class="c-checkbox-list">
                                <div class="c-checkbox">
                                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} class="c-check">
                                    <label for="remember">
                                        <span></span>
                                        <span class="check"></span>
                                        <span class="box"></span>
                                        Se souvenir de moi</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group c-margin-t-40">
                            <div class="col-sm-offset-4 col-md-8">
                                <button type="submit" class="btn c-theme-btn c-btn-square c-btn-uppercase c-btn-bold"><i class="fa fa-check"></i> Connexion</button>
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        Mot de passe oublié ?
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-5" style="background: rgba(124,124,124,0.85); border: 1px solid #0d0e0f; border-radius: 5px; padding-top: 10px; padding-bottom: 10px;">
                    <h3 class="c-font-white">Inscription</h3>
                    <form id="formRegister" action="{{ route('register') }}" method="post">
                        @csrf
                        <x-input
                            name="name"
                            type="text"
                            icon="fa fa-user"
                            label="Identité"
                            required="true"
                            labeled="false"
                        ></x-input>
                        <x-input
                            name="email"
                            type="email"
                            icon="fa fa-envelope"
                            label="Email"
                            required="true"
                            labeled="false"
                        ></x-input>
                        <x-input
                            name="password"
                            type="password"
                            icon="fa fa-key"
                            label="Mot de passe"
                            required="true"
                            labeled="false"
                        ></x-input>
                        <x-input
                            name="password_confirmation"
                            type="password"
                            icon="fa fa-key"
                            label="Confirmation du mot de passe"
                            required="true"
                            labeled="false"
                        ></x-input>
                        <div class="form-group c-margin-t-40">
                            <div class="col-sm-offset-4 col-md-8">
                                <button type="submit" class="btn c-theme-btn c-btn-square c-btn-uppercase c-btn-bold"><i class="fa fa-check"></i> Enregistrer</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

<script type="text/javascript">

</script>
