@extends('front.layout.app')

@section('content')
    <div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
        <div class="container">
            <div class="c-page-title c-pull-left">
                <h3 class="c-font-uppercase c-font-sbold">Récupération de votre mot de passe</h3>
            </div>
            <ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
                <li><a href="{{ route('login') }}">Mon Compte</a></li>
                <li>/</li>
                <li class="c-state_active">Récupération du mot de passe</li>

            </ul>
        </div>
    </div>

    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <form action="{{ route('password.email') }}" method="POST">
                @csrf
                <div class="form-group has-feedback @error('email') has-error @enderror">
                    <input type="email" class="form-control c-square c-theme input-lg" placeholder="Email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    <span class="fa fa-envelope form-control-feedback c-font-grey"></span>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group c-margin-t-40">
                    <div class="col-sm-offset-4 col-md-8">
                        <button type="submit" class="btn c-theme-btn c-btn-square c-btn-uppercase c-btn-bold"><i class="fa fa-check"></i> Envoyer le lien de renouvellement</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
