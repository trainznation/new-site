<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en"  >
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>{{ env("APP_NAME") }}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    {{-- <meta name="csrf-token" content="{{ csrf_token() }}"> --}}
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700&amp;subset=all' rel='stylesheet' type='text/css'>
    <link href="/assets/front/plugins/socicon/socicon.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/front/plugins/bootstrap-social/bootstrap-social.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/front/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/front/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/front/plugins/animate/animate.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/front/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN: BASE PLUGINS  -->
    <link href="/assets/front/plugins/revo-slider/css/settings.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/front/plugins/revo-slider/css/layers.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/front/plugins/revo-slider/css/navigation.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/front/plugins/cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/front/plugins/owl-carousel/assets/owl.carousel.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/front/plugins/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/front/plugins/slider-for-bootstrap/css/slider.css" rel="stylesheet" type="text/css"/>
    <!-- END: BASE PLUGINS -->


    <!-- BEGIN THEME STYLES -->
    <link href="/assets/front/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/front/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
    <link href="/assets/front/css/themes/default.css" rel="stylesheet" id="style_theme" type="text/css"/>
    <link href="/assets/front/css/custom.css" rel="stylesheet" type="text/css"/>
    @yield("styles")
    <!-- END THEME STYLES -->

    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
</head>
<body class="c-layout-header-fixed c-layout-header-6-topbar">

<!-- BEGIN: LAYOUT/HEADERS/HEADER-2 -->
@include("front.layout.include.header")
@include("front.layout.include.auth")

<!-- BEGIN: PAGE CONTAINER -->
<div class="c-layout-page">
    @yield("content")
</div>
<!-- END: PAGE CONTAINER -->

@include("front.layout.include.footer")
<!-- BEGIN: LAYOUT/FOOTERS/GO2TOP -->
<div class="c-layout-go2top">
    <i class="icon-arrow-up"></i>
</div><!-- END: LAYOUT/FOOTERS/GO2TOP -->

<!-- BEGIN: LAYOUT/BASE/BOTTOM -->
<!-- BEGIN: CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="/assets/front/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="/assets/front/plugins/jquery.min.js" type="text/javascript" ></script>
<script src="/assets/front/plugins/jquery-migrate.min.js" type="text/javascript" ></script>
<script src="/assets/front/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript" ></script>
<script src="/assets/front/plugins/jquery.easing.min.js" type="text/javascript" ></script>
<script src="/assets/front/plugins/reveal-animate/wow.js" type="text/javascript" ></script>
<script src="/assets/front/js/scripts/reveal-animate/reveal-animate.js" type="text/javascript" ></script>
<script src="https://js.pusher.com/7.0/pusher.min.js"></script>

<!-- END: CORE PLUGINS -->

<!-- BEGIN: LAYOUT PLUGINS -->
<script src="/assets/front/plugins/revo-slider/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
<script src="/assets/front/plugins/revo-slider/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
<script src="/assets/front/plugins/revo-slider/js/extensions/revolution.extension.slideanims.min.js" type="text/javascript"></script>
<script src="/assets/front/plugins/revo-slider/js/extensions/revolution.extension.layeranimation.min.js" type="text/javascript"></script>
<script src="/assets/front/plugins/revo-slider/js/extensions/revolution.extension.navigation.min.js" type="text/javascript"></script>
<script src="/assets/front/plugins/revo-slider/js/extensions/revolution.extension.video.min.js" type="text/javascript"></script>
<script src="/assets/front/plugins/revo-slider/js/extensions/revolution.extension.parallax.min.js" type="text/javascript"></script>
<script src="/assets/front/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
<script src="/assets/front/plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
<script src="/assets/front/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="/assets/front/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="/assets/front/plugins/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
<script src="/assets/front/plugins/smooth-scroll/jquery.smooth-scroll.js" type="text/javascript"></script>
<script src="/assets/front/plugins/typed/typed.min.js" type="text/javascript"></script>
<script src="/assets/front/plugins/slider-for-bootstrap/js/bootstrap-slider.js" type="text/javascript"></script>
<script src="/assets/front/plugins/js-cookie/js.cookie.js" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" type="text/javascript"></script>
<!-- END: LAYOUT PLUGINS -->

<!-- BEGIN: THEME SCRIPTS -->
<script src="/assets/front/js/components.js" type="text/javascript"></script>
<script src="/assets/front/js/components-shop.js" type="text/javascript"></script>
<script src="/assets/front/js/app.js" type="text/javascript"></script>
<script src="/assets/front/js/pusher.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        App.init(); // init core
        pusherInit.init();
    });
</script>
<!-- END: THEME SCRIPTS -->
@yield("scripts")
<!-- END: LAYOUT/BASE/BOTTOM -->
</body>
</html>
