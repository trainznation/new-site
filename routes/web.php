<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(["namespace" => "Front"], function () {
    Route::get('/', ["as" => "Front.home", "uses" => "HomeController@index"]);
    Route::post('/', ["as" => "Front.postSearch", "uses" => "HomeController@postSearch"]);
});
Auth::routes();

Route::get('logout', ["as" => "Account.logout", "uses" => "Auth\LoginController@logout"]);
