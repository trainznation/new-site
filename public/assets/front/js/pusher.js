let channel;
let pusherInit = function () {
    let handleInit = function () {
        Pusher.logToConsole = true;

        let pusher = new Pusher('8329973bd96c89b3c48c', {
            cluster: "eu"
        });

        channel = pusher.subscribe('frontend-general')
    };

    let handleBind = function () {
        channel.bind('registered-welcome', function (data) {
            toastr.success(data.message, "Bienvenue sur Trainznation")
        })
    };

    return {
        init: function () {
            handleInit()
            handleBind()
        }
    }
}();
